/**
 * @file kodak360cam_node.cpp
 * @brief ROS Nodelet of the kodak360cam_node.
 * @date 2016/05/26
 * @author Kensei Demura
 */


/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016 Kensei Demura
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the author nor other contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/


#include "KodakSP360CameraController.cpp"
#include <nodelet/nodelet.h>
#include <boost/thread.hpp>

#include <pluginlib/class_list_macros.h>

namespace kodak360cam{
  /**
   * @brief nodelet version of kodak360cam_node
   */
  class Kodak360CamNodelet : public nodelet::Nodelet {
  private:
    bool is_running;
    /**
     * @brief Thread object for main loop.
     */
    boost::shared_ptr<boost::thread> thread;

    /**
     * @brief Shared Object of Kodak SP 360 Camera Controller.
     */
    boost::shared_ptr<KodakSP360CameraController> kodaksp360;
    
    /**
     * @brief Start capturing/publishing thread.
     */
    virtual void onInit() {
      boost::shared_ptr<KodakSP360CameraController> kodaksp360_ptr(new KodakSP360CameraController(getPrivateNodeHandle()));
      kodaksp360 = kodaksp360_ptr;
      is_running = true;
      thread = boost::shared_ptr<boost::thread>
	(new boost::thread(boost::bind(&Kodak360CamNodelet::main, this)));
    }
    
    /**
     * @brief capture and publish.
     */
    void main(){
      while(is_running){
	kodaksp360->MainLoop();
      }
    }    
  
    
  public:
    
    explicit Kodak360CamNodelet(){
      is_running = false;
    }
    
    ~Kodak360CamNodelet(){
      if (is_running) {
	is_running = false;
	thread->join();
      }
    }  
  };
}

PLUGINLIB_DECLARE_CLASS(kodak360cam,Kodak360CamNodelet,kodak360cam::Kodak360CamNodelet,nodelet::Nodelet);

