/**
 * @file KodakSP3604KController.cpp
 * @brief Class for controlling Kodak SP360 Camera.
 * @date 2016/05/25
 * @author Kensei Demura
 */


/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016 Kensei Demura
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the author nor other contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "KodakSP360CameraController.h"  // NOLINT


/*
 * Constructor.
 * @param n NodeHandle object to give parameters for camera control.
 */
KodakSP360CameraController::KodakSP360CameraController(ros::NodeHandle& n):
  nh(n),
  device(0),
  fps(30),
  width(1280),
  height(720),
  camera_name("Kodak SP360 4K"),
  calibration_matches(true) {
  /**
   *Setup image transport, publisher, and camera info.
   */
  it = new image_transport::ImageTransport(nh);
  image_pub = it->advertiseCamera("/kodaksp360/image_raw", 1);
  cinfo = new camera_info_manager::CameraInfoManager(nh);

  /**
   *Setup NodeHandle parameters.
   */

  nh.getParam("device",device);
  nh.getParam("fps", fps);
  nh.getParam("width", width);
  nh.getParam("height", height);

  ROS_INFO("device : %d, fps : %d, width : %d, height :  %d", device,fps, width, height);

  if(device < 0){
    ROS_INFO("Device number : %d", device);
    ROS_INFO("Device Number not specified, setting fallback value.");
    device = 0;
  }
  if(fps <= 0) {
    ROS_INFO("FPS not specified, setting fallback value.");
    fps = 30;
  }

  if(width <= 0) {
    ROS_INFO("Width not specified, setting fallback value.");
    width = 1280;
  }
  if(height <= 0) {
    ROS_INFO("Height not specified, setting fallback value.");
    height = 720;
  }
}

/**
 * Main Loop for the Kodak SP360 Camera Control.
 */
void KodakSP360CameraController::MainLoop() {
  ROS_INFO("setting camera device : %d", device);
  cv::VideoCapture cap(device);
  /**
   * set capacities
   */
  cap.set(CV_CAP_PROP_FRAME_WIDTH, width);
  cap.set(CV_CAP_PROP_FRAME_HEIGHT, height);
  cap.set(CV_CAP_PROP_FPS, fps);

  bool result = cinfo->setCameraName(camera_name);
  calibration_matches = true;
  ros::Rate loop_rate(fps);

  while (nh.ok()) {
    cv::Mat frame;
    cap >> frame;
    int key = cv::waitKey(1);
    sensor_msgs::ImagePtr msg =
      cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();
    publish(msg);
    ros::spinOnce();
    loop_rate.sleep();
  }
}

void KodakSP360CameraController::publish(const sensor_msgs::ImagePtr &image) {
  sensor_msgs::CameraInfoPtr
    ci(new sensor_msgs::CameraInfo(cinfo->getCameraInfo()));

  bool checkCameraInfo =
    ( (width == image->width) && (height == image->height) );
  if (!checkCameraInfo) {
    calibration_matches = false;
    ci.reset(new sensor_msgs::CameraInfo());
    ci->height = image->height;
    ci->width = image->width;
  } else if (!calibration_matches) {
    calibration_matches = true;
  }
  ci->header.frame_id = "kodaksp360_frame";
  ci->header.stamp = image->header.stamp;
  image_pub.publish(image, ci);
}
