# kodaksp360_node  
ROS node for publishing KodakSP360 image stream  

## Supported Cameras  
Kodak SP 360 4K  
![KodakSP3604K](docs/kodaksp3604k.jpg)  
Kodak SP 360 4K VR360  
![KodakSP3604KVR360](docs/kodaksp3604kvr360.jpg)  
  
  
## 3D Mount for robots  
Coming Soon!  
  
  
## Documentation  
[Doxygen](docs/html/index.html)  
  
  
## Camera Calibrations  
Add sensor_msgs/CameraInfo Topic  
Camera calibration settings : [camera.yaml](cfg/kodaksp3604k.yaml)  
  
  
## ROS API  
  
### Published Topics  
kodaksp360/image_raw[ (sensor_msgs/Image)](http://docs.ros.org/jade/api/sensor_msgs/html/msg/Image.html)  
kodaksp360/CameraInfo[ (sensor_msgs/CameraInfo)](http://docs.ros.org/api/sensor_msgs/html/msg/CameraInfo.html)  
  
### Parameters  
~width(int, default: 1920)  
~height(int, default: 1080)  
~fps(int, default: 15)  
~device(int, default: 1)  
  
## How to run  
### ROS Node  
rosrun kodak360cam_node kodak360cam_node [Options]  
  
### ROS Launch  
  
#### single camera launcher  
```roslaunch kodak360cam_node kodak_nodelet.launch```  
   
#### double camera launcher
```roslaunch kodak360cam_node kodak_double.launch```  
  
#### Kodak SP360 VR360 Camera Launcher  
```roslaunch kodak360cam_node kodakvr_4k.launch```  
  
#### Kodak SP360 VR360 Camera Launcher (1080p30FPS)  
```roslaunch kodak360cam_node kodakvr_fullhd.launch```  

#### Kodak SP360 VR360 Camera Launcher(8K))  
```roslaunch kodak360cam_node kodakvr_8k.launch```  


  

## ROS launch file  
kodak_nodelet.launch : single camera launcher  
kodak_double.launch : dual camera launcher
kodak_superres.launch : 1440p camera launcher
kodak_4k.launch : 4K camera launcher(available in Linux Kernel 4.4)  
  
##Data Transfer Minimalization  
Using nodelet to do data transfer minimalization.  
  
##4K Support  
supporting 4K streaming in Linux Kernel 4.4  
  
##Testing Suite  
Static Analysis : add cppcheck and cpplint checking  
Testing : TODO  
