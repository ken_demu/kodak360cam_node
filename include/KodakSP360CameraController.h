/**
 * @file KodakSP360CameraController.h
 * @brief Class of the Kodak Camera Controller. Writing field defenition here.
 * @date 2016/05/25
 * @author Kensei Demura
 */

/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016 Kensei Demura
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
x *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the author nor other contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef INCLUDE_KODAKSP360CAMERACONTROLLER_H_
#define INCLUDE_KODAKSP360CAMERACONTROLLER_H_

#include <string> // NOLINT
#include <stdint.h> // NOLINT
#include <boost/thread/mutex.hpp> // NOLINT
#include <ros/ros.h> // NOLINT
#include <image_transport/image_transport.h> // NOLINT
#include <cv_bridge/cv_bridge.h> // NOLINT
#include <sensor_msgs/image_encodings.h>  // NOLINT
#include <sensor_msgs/Image.h> // NOLINT
#include <sensor_msgs/CameraInfo.h> // NOLINT
#include <camera_info_manager/camera_info_manager.h> // NOLINT
#include <opencv2/opencv.hpp> // NOLINT

class KodakSP360CameraController{
  /**
   * Class for controlling Kodak SP360 Camera.
   */


 private:
  /** camera capacites */
  int32_t device;
  int32_t width;
  int32_t height;
  int32_t fps;
  const std::string camera_name;


  /** ROS config*/
  ros::NodeHandle nh;

  /** camera calibration info*/
  camera_info_manager::CameraInfoManager* cinfo;
  bool calibration_matches;


  /** image transport interface*/
  image_transport::ImageTransport* it;
  image_transport::CameraPublisher image_pub;


  /**
   *publish Kodak SP 360 video stream with Camera Info.
   *@param image camera image
   */
  void publish(const sensor_msgs::ImagePtr &image);


 public:
  /** 
   *Constructor.
   */
  explicit KodakSP360CameraController(ros::NodeHandle& n);

  /**
   * Main Loop for the Kodak SP360 Camera Control.
   */
  void MainLoop();
};

#endif  // INCLUDE_KODAKSP360CAMERACONTROLLER_H_
